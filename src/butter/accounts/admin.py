from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.core.exceptions import ObjectDoesNotExist

from .models import User


class UserAdmin(DjangoUserAdmin):
    list_display = (
        'username', 'email', 'first_name', 'last_name', 'is_staff',
        'is_superuser', 'has_agreement'
    )
    fieldsets = DjangoUserAdmin.fieldsets + (
        (None, {'fields': ('street', 'post_code')}),
    )

    # TODO: Add `has_agreement` to `list_filter` fields.
    def has_agreement(self, obj):
        try:
            obj.agreement
            return True
        except ObjectDoesNotExist:
            return False
    has_agreement.boolean = True
    has_agreement.admin_order_field = 'agreement'
    has_agreement.short_description = 'agreement'


admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
