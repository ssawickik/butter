from django.core.management.commands.test import Command as DjangoCommand
from django.conf import settings


class Command(DjangoCommand):
    def handle(self, *args, **options):
        super(Command, self).handle(
            *(settings.INSTALLED_APPS if not bool(args) else args),
            **options
        )
