from django.db import models
from django.db.models import ProtectedError
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.conf import settings
from django.core.exceptions import ValidationError
from django.template import Template, Context
from django.utils.translation import ugettext_lazy as _


class AgreementTemplateModel(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    context = models.TextField(blank=False, null=False)
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Agreement template')
        verbose_name_plural = _('Agreement templates')

    def save(self, *args, **kwargs):
        agreement_templates = self.__class__.objects.all()
        if self.active:
            agreement_templates.update(active=False)
        elif not agreement_templates.count():
            self.active = True
        elif not (
            agreement_templates.filter(active=True).exclude(id=self.pk).count()
        ):
            raise ValidationError(_(
                'There must be an active template.'
                ' To change active template, simply mark another template'
                ' as active.'
            ))
        super().save(*args, **kwargs)


class AgreementModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    first_name = models.CharField(max_length=30, blank=True, default='')
    last_name = models.CharField(max_length=150, blank=True, default='')
    street = models.CharField(max_length=50, blank=True, default='')
    post_code = models.CharField(max_length=50, blank=True, default='')
    template = models.ForeignKey(
        AgreementTemplateModel,
        related_name='agreements',
        on_delete=models.PROTECT,
        blank=False,
        null=False,
    )
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='agreement',
        blank=False,
        null=False,
    )

    def __str__(self):
        return '%s - %s' % (self.user.username, self.template.name)

    class Meta:
        verbose_name = _('Agreement')
        verbose_name_plural = _('Agreements')

    @property
    def as_html(self):
        template = Template(self.template.context)
        context = Context({
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'street': self.user.street,
            'post_code': self.user.post_code,
            'date': self.created_at,
        })
        return template.render(context)

    def save(self, force=False, *args, **kwargs):
        if self.pk and not force:
            raise ProtectedError(_('Signed agreements cannot be changed.'), self)
        self.first_name = self.first_name or self.user.first_name
        self.last_name = self.last_name or self.user.last_name
        self.street = self.street or self.user.street
        self.post_code = self.post_code or self.user.post_code
        try:
            self.template
        except AgreementTemplateModel.DoesNotExist:
            self.template = AgreementTemplateModel.objects.get(active=True)
        super().save(*args, **kwargs)


@receiver(
    pre_delete,
    sender=AgreementTemplateModel,
    dispatch_uid='pre_delete_signal'
)
def protect_delete_active_agreement_template(sender, instance, using, **kwargs):
    if instance.active:
        raise ProtectedError(_(
            'Active agreement template cannot be deleted.'
            ' First mark another template as active and then try again.'
        ), instance)
