from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED
from rest_framework.mixins import ListModelMixin, CreateModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.renderers import StaticHTMLRenderer
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .models import AgreementModel
from .serializers import AgreementSerializer


class AgreementViewSet(ListModelMixin, CreateModelMixin, GenericViewSet):
    renderer_classes = [StaticHTMLRenderer]
    queryset = AgreementModel.objects
    serializer_class = AgreementSerializer
    permission_classes = (IsAuthenticated,)
    lookup_field = 'user'

    def get_object(self):
        self.kwargs['user'] = self.request.user
        return super().get_object()

    def list(self, request, *args, **kwargs):
        return self._get_html_response()

    def create(self, *args, **kwargs):
        self.request.data['user'] = self.request.user.pk
        super().create(*args, **kwargs)
        return self._get_html_response(HTTP_201_CREATED)

    def _get_html_response(self, status=None):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data['html'], status=status or HTTP_200_OK)
