from functools import wraps

from django.db.models import ProtectedError
from django.contrib import admin, messages
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.urls import reverse

from .models import AgreementModel, AgreementTemplateModel


def handle_exceptions(func):
    @wraps(func)
    def wrapper(self, request, *args, **kwargs):
        try:
            return func(self, request, *args, **kwargs)
        except (ProtectedError, ValidationError) as exc:
            self.message_user(request, str(exc), messages.ERROR)
            opts = self.model._meta
            if 'object_id' in kwargs:
                return_url = reverse(
                    'admin:%s_%s_change' % (opts.app_label, opts.model_name),
                    args=(kwargs['object_id'],),
                    current_app=self.admin_site.name,
                )
            else:
                return_url = reverse(
                    'admin:%s_%s_changelist' % (opts.app_label, opts.model_name),
                    current_app=self.admin_site.name,
                )
            return HttpResponseRedirect(return_url)
    return wrapper


class AgreementAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'first_name',
        'last_name',
        'street',
        'post_code',
        'template',
        'created_at',
    )
    fieldsets = (
        (None, {
            'fields': ('user', 'template')
        }),
        ('User current data', {
            'fields': (
                'user_first_name',
                'user_last_name',
                'user_street',
                'user_post_code'
            ),
        }),
        ('Agreement data', {
            'fields': ('first_name', 'last_name', 'street', 'post_code'),
        }),
    )
    dynamic_readonly_fields = ['first_name', 'last_name', 'street', 'post_code']
    readonly_fields = [
        'user_first_name', 'user_last_name', 'user_street', 'user_post_code'
    ]
    search_fields = ('first_name', 'last_name', 'street', 'post_code')
    list_filter = ('template',)

    def username(self, obj):
        return obj.user.username
    username.short_description = 'user'
    username.admin_order_field = 'user__username'

    def user_first_name(self, obj):
        return obj.user.first_name

    def user_last_name(self, obj):
        return obj.user.last_name

    def user_street(self, obj):
        return obj.user.street

    def user_post_code(self, obj):
        return obj.user.post_code

    def get_readonly_fields(self, request, obj=None, *args, **kwargs):
        readonly_fields = super().get_readonly_fields(
            request, *args, obj=obj, **kwargs
        )
        if request.user.is_superuser:
            return readonly_fields
        return readonly_fields + self.dynamic_readonly_fields

    def has_delete_permission(self, request, *args, **kwargs):
        return bool(request.user.is_superuser)

    def has_change_permission(self, request, *args, **kwargs):
        return bool(request.user.is_superuser)

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        if db_field.name == 'template':
            agreement_template = AgreementTemplateModel.objects.filter(
                active=True
            )
            if agreement_template:
                kwargs['initial'] = agreement_template[0]
        return super().formfield_for_dbfield(db_field, request, **kwargs)

    def save_model(self, request, obj, *args, **kwargs):
        obj.save(force=bool(request.user.is_superuser))


class AgreementTemplateAdmin(admin.ModelAdmin):
    list_display = ('name', 'active')

    @handle_exceptions
    def change_view(self, *args, **kwargs):
        return super().change_view(*args, **kwargs)

    @handle_exceptions
    def delete_view(self, *args, **kwargs):
        return super().delete_view(*args, **kwargs)

    @handle_exceptions
    def response_change(self, *args, **kwargs):
        return super().response_change(*args, **kwargs)

    @handle_exceptions
    def response_action(self, *args, **kwargs):
        return super().response_action(*args, **kwargs)


admin.site.register(AgreementModel, AgreementAdmin)
admin.site.register(AgreementTemplateModel, AgreementTemplateAdmin)
