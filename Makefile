# Add the following 'help' target to your Makefile
# And add help text after each target name starting with '\#\#'

help:           		## Show this help.
	@fgrep -h '##' $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

pull:				## Pull docker images.
	docker-compose pull $(ARGS)

build:				## Build docker containers.
	docker-compose build $(ARGS) && docker-compose run -v ${PWD}:/sources app sh -c 'pip3 install -e .'

migrate:			## Migrate database from docker container.
	docker-compose run --rm --service-ports -v ${PWD}:/sources app sh -c './bin/wait_for_db.sh && butter migrate'

install: pull build migrate	## Prepare app with all dependencies.
	@echo 'The installation has been completed!'

run:      			## Run app in docker container. (Development mode)
	docker-compose run --service-ports -v ${PWD}:/sources app sh -c './bin/wait_for_db.sh && butter runserver 0.0.0.0:8000'

test:				## Run tests in docker container.
	docker-compose run --no-deps -v ${PWD}:/sources app sh -c 'butter test $(ARGS)'

shell:				## Open django shell in container.
	docker-compose run --service-ports -v ${PWD}:/sources app sh -c './bin/wait_for_db.sh && butter shell'

bash:				## Run app container with bash.
	docker-compose run --service-ports -v ${PWD}:/sources app sh -c './bin/wait_for_db.sh && /bin/bash'

cmd:				## Execute given command in app container.
	docker-compose run --service-ports -v ${PWD}:/sources app sh -c './bin/wait_for_db.sh && $(CMD)'

dumpdemodata:			## Dump demo data to `src/butter/demodata.json`.
	docker-compose run --service-ports -v ${PWD}:/sources app sh -c './bin/wait_for_db.sh && butter dumpdata -e contenttypes -e sessions > src/butter/demodata.json'

loaddemodata:			## Load demo data from `src/butter/demodata.json`.
	docker-compose run --service-ports -v ${PWD}:/sources app sh -c './bin/wait_for_db.sh && butter loaddata src/butter/demodata.json'

lint:     			## Run linter in container. (flake8)
	docker-compose run --no-deps --service-ports -v ${PWD}:/sources app sh -c 'flake8 ./src/butter/'

todo:				## Show `# TODO:` comments from the entire project.
	@grep '# TODO:' src/butter -r
