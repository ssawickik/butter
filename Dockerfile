FROM python:3.8

RUN apt-get update && apt-get install -y \
    netcat \
    postgresql-client \
    postgresql-client-common

ADD . /sources

WORKDIR /sources

RUN pip3 install -r requirements.txt

EXPOSE 8000

CMD ["./bin/entrypoint.sh"]