OVERVIEW
========
Butter Django Engineer - Technical Test.

```Notice``` There is a one thing that I'm not happy about but I haven't found a better solution. The agreement html templates are stored in the database. The reason why I decided to do this are two requirements specified in the email.

* signed agreements cannot be changed
* disk space optimization

At first glance, the problem seems simple, we need to reduce amount of disk space which means that we cannot store fully generated agreements so, we create 2 tables one for agreements and the second one for agreement templates. The "Template" means we should create the "html" file however, here is the trap. If for some reason the file disappears or will be changed we are not be able to restore the generated agreements. That's why I decided to store the html content in the database and protect them from delete. The "html" content stored in the database sounds awful however, the awful solutions sounds better then a disaster.


REQUIREMENTS
------------

* python >= 3.8
* docker >= 19.03.5
* docker-compose >= 1.25.3


Install
-------

    make install
    make loaddemodata

```Notice``` For build only, use command `make build` because there are some problems with link `*.egg-info` inside docker container. Otherwise you will have to run `docker-compose run -v ${PWD}:/sources app sh -c 'pip3 install -e .'` after `docker-compose build`. For get more details see [python docker disappearing egg-info](https://jbhannah.net/articles/python-docker-disappearing-egg-info/).


Usage
-----

    make run


Tests
-----

    make test


Endpoints
---------

##### Agreement

The endpoint works for the logged in user and return 'text/html' response. If successful, 'GET' will return '200' and 'POST' will return '201'. Otherwise, if the user has not yet signed agreement 'GET' will return' 404' and if the user has already signed agreement 'POST' will return '400'.

* Get agreement : `POST /api/v1/agreements/`
* Sign agreement : `POST /api/v1/agreements/`

##### Auth
Configured backends `BasicAuthentication`, `SessionAuthentication`, `TokenAuthentication`

* Obtain auth token : `POST /api/v1/api-token-auth/`
* Rest framework urls : `/api/v1/api-auth/`


Accounts
--------

**admin** the superuser who can do almost anything. This account can modify signed contracts. (backdoor for quick fixes)

    admin / admin

**moderator** can add/remove/change agreement templates, but cannot change signed agreements

    moderator / moderator

**user** an regular users

    abby_blackburn / userpassword
    louis_robson / userpassword
    nathan_tyler / userpassword
    rhys_waters / userpassword
    scott_hewitt / userpassword
    yasmin_holmes / userpassword


TODO
----

- Add more tests to make sure that everything works fine.
- Add 'has_agreement' to 'list_filter' fields in 'accounts/admin.py:UserAdmin'.
- Configure django test database and improve docker files to use it for running tests.
- Configure logs file handler (RotatingFileHandler)
